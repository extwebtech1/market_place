class Api::V1::UsersController < ApplicationController
  before_action :set_user, only: %i[show update destroy]
  before_action :check_owner, only: %i[update destroy]

  def index
    @users = User.all
    render json: @users
  end

  def show
    options = { include: [:products] }
    render json: UserSerializer.new(@user, options).serializable_hash
  end

  def create
    @user = User.new(user_params)
    if @user.save
      render json: UserSerializer.new(@user).serializable_hash, status: :created
      else
      render json: @user.errors, status: :unprocessable_entity
    end
  end


  def login
    @user = User.find_by_email(params[:email]) 
      if @user&.authenticate(params[:password])        
        render json: { 
        token: JsonWebToken.encode(user_id: @user.id),
        data: @user        
        }
        else
        render json:{
        message: "email or password is incorrect"
        }
      end
  end

  def update
    if @user.update(user_params)
      render json: UserSerializer.new(@user).serializable_hash
      else
      render json: @user.errors, status: :unprocessable_entity
    end
  end



  def destroy
      user = User.find_by(id: params[:id])
      if user.present?
        user.delete
        render json: {message: 'User deleted successfully.'}
      else
        render json: {message: 'Record not found.'}
    end
  end

  private



  def set_user
    @user = User.find(params[:id])
  end

  def check_owner
    head :forbidden unless @user.id == current_user&.id
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
