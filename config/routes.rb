Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
    end
  end
  namespace :api, defaults: { format: :json } do 
    namespace :v1 do
      resources :users, only: %i[ create update destroy index ] do
      post :login, on: :collection
    end
      # resources :tokens, only: [:create]
      resources :products 
      resources :orders, only: %i[index show create]
    end
  end
end
