require 'rails_helper'

RSpec.describe "Orders", type: :request do
 describe "POST Create Order" do
  it "create Orders" do
    user1 = User.create(email: "vikas@4321gmail.com", password: "vikas4321")
    post "/api/v1/orders", params: {order:{

                             total: "1.4", user_id: user1.id
    }
    },  headers: { token: JsonWebToken.encode(user_id: user1.id)}
      data = JSON.parse(response.body)                                  
      expect(JSON.parse(response.body)['total']).to eq("0.0")
      expect(response).to have_http_status(201)
    end
  end
end
