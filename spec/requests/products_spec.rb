require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "POST Create product" do
    it "create product" do
      user1 = User.create(email: "vikas@4321gmail.com", password: "vikas4321")
      post "/api/v1/products", params:{product:{
                                                                
           title:"Hello", price:"200", published:"true", quantity: "1", user_id: user1.id
    }
      }, headers: { token: JsonWebToken.encode(user_id: user1.id)}
   
      data = JSON.parse(response.body)['data']                                   
      expect(JSON.parse(response.body)['data']['attributes']['title']).to eq("Hello")
      expect(response).to have_http_status(201)
    end
  end

  describe "GET particular product" do
    it "get particular product" do

      get "/api/v1/products/#{@products_id}"
      data = JSON.parse(response.body)
      expect(response).to have_http_status(200)
    end
  end


  describe "PUT Product" do
    it "update product" do
      user1 = User.create(email: "chauras2vi21k@gmail.com", password: "vikas")
      product = Product.create(title:"Hello", price:"200", published:"true", quantity:"1", user_id: user1.id)
      put "/api/v1/products/#{product.id}", headers: { token: JsonWebToken.encode(user_id: user1.id)}, params: {
                                                                                                        "product": {
                                                                                                        "title": "hello viru",
                                                                                                          }
                                                                                                        }
      data = JSON.parse(response.body)
      expect(data['data']['attributes']['title']).to eq('hello viru')
      expect(response).to have_http_status(200)
    end
  end

  describe "DELETE  Product" do
    it "Delete product" do
      user = User.create(email: "vikas@gmail.com", password: "vikas@121")
      product = Product.create(title: "phone", price: 39999, quantity: 4, published: true, user_id: user.id)
      delete "/api/v1/products/#{product.id}", headers: {token: JsonWebToken.encode(user_id: user.id)}

      data = JSON.parse(response.body)
      expect(data['message']).to eq('Product deleted successfully.')
      expect(response).to have_http_status(200)
    end
  end
end
