require 'rails_helper'

RSpec.describe "Users", type: :request do
  describe "POST Create User" do
    it "create user" do
      post "/api/v1/users", params:{ user: { email: "chauras2vi21kas1998@gmail.com", password: "chaurasia123"}}
     
    data = JSON.parse(response.body)['data']
    expect(JSON.parse(response.body)['data']['attributes']["email"]).to eq("chauras2vi21kas1998@gmail.com")
      expect(response).to have_http_status(201)
    end
  end


  describe "POST Create User" do
    it "user not create" do
      us = User.create(email: "chauras28@gmail.com", password: "vikas@123")
      post "/api/v1/users",params: { user: { email: "chauras28@gmail.com", password: "vikas@123" }}

      data = JSON.parse(response.body)
      expect(response).to have_http_status(422)
    end
  end

  describe "GET particular user" do
    it "get particular user" do
      get "/api/v1/users/#{@users_id}"
      data = JSON.parse(response.body)
      expect(response).to have_http_status(200)
    end
  end

  describe "POST #Login" do
    it "When email already" do
      user1 = User.create(email: "vikas@4321gmail.com", password: "vikas4321")
      post "/api/v1/users", params:{ user: { email: "vikas@4321gmail.com", password: "vikas4321"}}
       data = JSON.parse(response.body)['errors']
      expect(response).to have_http_status(422)
    end
  end
  
  describe "POST  User #Login" do
    it "create login" do
      user1 = User.create(email: "vikas@4321gmail.com", password: "vikas4321")

      post "/api/v1/users/login", params: {email: "vikas@4321gmail.com", password: "vikas4321"
    
      }, headers: { token: JsonWebToken.encode(user_id: user1.id)}
      data = JSON.parse(response.body)['data']
      expect(JSON.parse(response.body)['data']['email']).to eq("vikas@4321gmail.com")
      expect(response).to have_http_status(200)
    end
  end


  describe "PUT  User #update" do
      it "update user" do
       # byebug
        user1 = User.create(email: "vikas@4321gmail.com", password: "vikas4321", password_confirmation: "vikas4321")
        put "/api/v1/users/#{user1.id}", headers: { token: JsonWebToken.encode(user_id: user1.id)},  params: {
                                                                                                         "user":{
                                                                                                          "email": "vikas432@gmail.com"
                                                                                                        }
                                                                                                      }
        data = JSON.parse(response.body)
        expect(data['data']['attributes']['email']).to eq('vikas432@gmail.com')
        expect(response).to have_http_status(200)
      end
    end

  describe "DELETE User" do
    it "Delete user" do
      user1 = User.create(email: "vikas@4321gmail.com", password: "vikas4321", password_confirmation: "vikas4321")
              delete "/api/v1/users/#{user1.id}", headers: { token: JsonWebToken.encode(user_id: user1.id)}
               data = JSON.parse(response.body)
      expect(data['message']).to eq('User deleted successfully.')
      expect(response).to have_http_status(200)
    end
  end
end


  