require 'rails_helper'

RSpec.describe User, type: :model do

  context "validation user" do
    it "ensure presence of some attributes" do
      user = User.create()
      expect(user.errors.messages[:email]).to eq(["is invalid"])
      expect(user.errors.messages[:password]).to eq(["can't be blank"])
    end
  end

   context "association test" do
    it "should has_many products " do
      t = User.reflect_on_association(:products)
      expect(t.macro).to eq(:has_many)
    end
  end

  context "association test" do
    it "should has_many orders " do
      t = User.reflect_on_association(:orders)
      expect(t.macro).to eq(:has_many)
    end
  end
end
