require 'rails_helper'

RSpec.describe Order, type: :model do
   context "association test" do
    it "should belongs_to user " do
      t = Order.reflect_on_association(:user)
      expect(t.macro).to eq(:belongs_to)
    end
  end
end
